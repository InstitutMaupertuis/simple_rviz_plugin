#include <atomic>
#include <ros/ros.h>
#include <ros/service.h>
#include <simple_node/GetMessage.h>

std::atomic<unsigned> i;

bool getMessageCallback(simple_node::GetMessage::Request &req,
                        simple_node::GetMessage::Response &res)
{
  ROS_INFO_STREAM("getMessageCallback: " << req.value);

  if (i == 0)
  {
    res.return_status = true;
    res.return_message = "Hello!\nThis service will fail next time you call it.";
  }
  else
  {
    res.return_status = false;
    res.return_message = "Uh oh, failure!\nThis service will succeed next time you call it.";
  }

  ++i;
  if (i > 1)
    i = 0;
  return true;
}

int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "simple_node");
  ros::NodeHandle nh;
  i = 0;
  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::ServiceServer service = nh.advertiseService("get_message", getMessageCallback);
  ros::waitForShutdown();
  spinner.stop();
  return 0;
}
